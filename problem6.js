function listOfAllCarsInTheSet(cars,carList){
    
   carList = new Set(carList);
    if(typeof cars!=='object' || typeof cars==='null'|| cars.length===0||typeof carList!=='object' || typeof carList==='null'|| carList.size===0){
        return [];
    }

    let allCarsOfAType =[];
    for(let car of carList){
        for(let i=0;i<cars.length;i++){
            if(cars[i].car_make=== car){
                allCarsOfAType.push(cars[i]);
            }
        }   
    }
    return allCarsOfAType;  

}


module.exports =listOfAllCarsInTheSet

