//no of cars older than given year
function carsOlderThanYear(cars,year1){
    
    if(!Number.isInteger(year1) || typeof cars !=='object' || typeof year1==='null' || typeof cars==='null' || cars.length===0){
        return []
    }
    let olderthanYear1 = []
    for(let i=0;i<cars.length;i++){
        if(cars[i].car_year<year1){

            //checking for all invalid year inputs
            let yearNow = new Date().getFullYear()           
            if(cars[i].car_year>yearNow && cars[i].car_year<1896){
                return [];
            }

            olderthanYear1.push(cars[i]);
        }
    }
    return olderthanYear1;
}
module.exports = carsOlderThanYear
