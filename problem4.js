function arrayOfCarYears(cars){
    
    if(typeof cars ==='object' || typeof cars==='null'||cars.length===0){
        return []
    }
    let carYear = []
    //add car models to an array
    for(let i=0;i<cars.length;i++){
        carYear.push(cars[i].car_year);
    }
    return carYear;
}

module.exports = arrayOfCarYears;